# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 09:59:31 2019

@author: Alisson Souza
"""
import visa

class ITC400X():
    def __init__(self):
        self.rm = visa.ResourceManager()
        self.ITC=self.rm.open_resource("USB0::0x1313::0x804A::M00312102::INSTR")
        print(self.ITC.query("*IDN?"))
        
    def close(self):
        self.ITC.close()
        
    def get_mode_CW(self):
        ret=self.ITC.query("SOUR1:FUNC:MODE?;SHAP?") #Queries the LD source function.
        return ret   
    
    def set_mode_CW(self,mode):
        # mode must be set as PULS or DC
         ret=self.ITC.write(f"SOUR1:FUNC:MODE CURR;SHAP {mode}") #Sets the LD source function of a LDC4000 Series or an ITC4000 Series instrument to constant current (CW) mode.
         #print(ret)

    def get_current_limit(self):
        ret=self.ITC.query("SOUR1:CURR:LIM?") #Queries the maximum settable LD source limit current of a LDC4000 Series or an ITC4000 Series instrument.
        return ret

    def set_current_limit(self,curr_lim):
        ret=self.ITC.write(f"SOUR1:CURR:LIM {curr_lim}")  ## high range mA
        #print(ret)
        
    def get_current(self):
        ret=self.ITC.query("SOUR1:CURR?")
        return ret
        
    def set_current(self,current):
        ret=self.ITC.write(f"SOUR1:CURR {current}")
        #print(ret)
        
    def set_LD_status(self,status):
        ret=self.ITC.write(f"OUTP1 {status}") #Switches the LD output on.
        #print(ret)

        
    def get_LD_status(self):
        ret=self.ITC.query("OUTP1?") #Queries the state of the LD output.
        return float(ret)
    
    def get_temp(self):
        ret=self.ITC.query("SOUR2:TEMP?")
        return float(ret)
        
    def set_temp(self,temp):
        ret=self.ITC.write(f"SOUR2:TEMP {temp}")
        #print(ret)
        
    def get_TEC_status(self):
        ret=self.ITC.query("OUTP2?") #Queries the state of the TEC output.
        return float(ret)
    
    def set_TEC_status(self,status):
        ret=self.ITC.write(f"OUTP2 {status}") #Switches the TEC output on.
        #print(ret)
    def set_thermopile_responsitivity(self,resp):
        self.ITC.write(f"SENS2:CORR:POW {resp}mV")
#        return float(ret)   
        
    def read_voltage(self):
        self.ITC.write("CONF:VOLT1")
        ret=float(self.ITC.query("READ?"))
        return ret    

    def read_current(self):
        self.ITC.write("CONF:CURR1") #
        ret=float(self.ITC.query("READ?"))
        return ret    
    def read_power(self):
        self.ITC.write("CONF:POW3")
        ret=float(self.ITC.query("READ?"))
        return ret    
     
    def read_temperature(self):
        self.ITC.write("CONF:TEMP")
        ret=float(self.ITC.query("READ?"))
        return ret  
    def set_duty_cycle(self,d):
        self.ITC.write(f"SOUR1:PULS:DCYCle {d}")
     
          

if __name__=="__main__":
    ld=ITC400X()
    print(ld.set_mode_CW('DC'))
    print(ld.get_mode_CW())
    print(ld.set_current_limit(0.400))
    print(ld.get_current_limit())
    print(ld.set_current(0.140))
    print(ld.get_current())
    print(ld.set_LD_status(0))
    print(ld.get_LD_status())
    print(ld.set_temp(25))
    print(ld.get_temp())
    print(ld.read_temperature())
    print(ld.set_TEC_status(0))
    print(ld.get_TEC_status())
    print(ld.read_voltage())
    print(ld.read_current())
    print(ld.read_power())
    ld.set_duty_cycle(11)

    ld.close()
