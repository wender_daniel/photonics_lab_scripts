# -*- coding: utf-8 -*-
"""
Laser characterization setup equipment API

@copyright Idea! Electronic Systems
@author Diogo de Azevedo Motta <diogo.motta@idea-ip.com>
@author Felipe Bizerra Fideles <felipe.fideles@idea-ip.com>
"""

import visa
#import time
#from pyvisa.constants import VI_FALSE, VI_ATTR_SUPPRESS_END_EN,VI_ATTR_SEND_END_EN


class Photodetector(object):
   '''External ThorLabs Photodetector's Class'''

   def __init__( self, resource, unit='DBM', bandwidth='LOW', visastr ='USB0::0x1313::0x8078::P0024381::INSTR'):
       self.rma = visa.ResourceManager()
       self.resource = visastr
       self.unit = unit
       self.bandwidth = bandwidth
       self.device = self.rma.open_resource(self.resource)
#
       # Inital Configuration
       self.set_unit(self.unit)
       self.set_bandwidth(self.bandwidth)
       
   def __del__(self):
       self.device.close()

   def get_idn(self):
       '''Retrieve device identification'''
       return self.device.query("*idn?")

   def get_power(self):
       '''Retrieve external photodetector's power'''
       while not self.get_status_event():
           continue

       value = float(self.device.query("MEAS?"))
       while value > 1000:
           value = float(self.device.query("MEAS?"))

       return value

   def set_unit(self, unit):
       '''Change external photodetector's unit'''
       if unit=='DBM':
           self.device.write("SENS:POW:UNIT DBM")
       elif unit=='W':
           self.device.write("SENS:POW:UNIT W")
       self.unit = unit

   def get_status_event(self):
       '''Read the event register'''
       value = self.device.query("STAT:MEAS:EVEN?")
       return bool(value)

   def set_bandwidth(self, bw):
       '''Set the measurement bandwidth to either LOW or HIGH'''
       if bw=='LOW':
           self.device.write("INP:PDI:FILT:LPAS:STAT 1")
       elif bw=='HIGH':
           self.device.write("INP:PDI:FILT:LPAS:STAT 0")
       self.bw = bw
       
if __name__=="__main__":
    pm=Photodetector('W','LOW')
    pm.set_unit('W')
    print(f'\nPower: {pm.get_power()}')
#    pm.set_bandwidth('HIGH')
      
