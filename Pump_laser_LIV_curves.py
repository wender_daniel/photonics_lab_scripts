# -*- coding: utf-8 -*-
"""
Created on Thu Nov  4 10:18:51 2021

@author: wender.daniel
"""

# General imports
import time, pickle
import numpy as np
import matplotlib.pyplot as plt

# Instruments imports
from equipapi.photodetector import thorlabs as tl_pm100
import ITC4001 

# Temperature
temp = 70  # ­°C
cos = '3'

save_file_name = "COS_FBG_prototype_" + cos + "_" + str(temp) + "C" + ".pkl"
file_path = "C:/Users/wender.daniel/Desktop/FBG_Pump_Prototypes/"
save_file_path = f"C:/Users/wender.daniel/Desktop/FBG_Pump_Prototypes/{save_file_name}"

curr_start = 0  # mA
curr_stop = 0.3
curr_n = 50
I = np.linspace(curr_start, curr_stop, curr_n)

print("Initializing instruments")
tl_itc = ITC4001.ITC400X()
tl_pm = tl_pm100.Photodetector('USB0::0x1313::0x8078::P0030326::INSTR', unit='W', bandwidth='LOW')


V = np.zeros(len(I))
P = np.zeros(len(I))
T = np.zeros(len(I))

tl_itc.set_temp(temp)
tl_itc.set_TEC_status(1)

print('Wait for Temperature Stabilization!')
while(abs(tl_itc.read_temperature() - temp) > 0.5):
    time.sleep(5)
    
time.sleep(2.0)

tl_itc.set_current(70)
tl_itc.set_LD_status(1)
time.sleep(5)

for i in range(len(I)):
    tl_itc.set_current(I[i])
    print(str(float(tl_itc.get_current())*1e3)[0:5] + " mA")
    time.sleep(0.5)
    P[i] = tl_pm.get_power()
    V[i] = tl_itc.read_voltage()
    T[i] = tl_itc.read_temperature()
   
    
tl_itc.set_LD_status(0)
#tl_itc.set_TEC_status(0)
    
data_dict = {'P': P, 'I': I, 'V': V, 'T': T}

plt.rcParams['figure.figsize'] = [10,8]
plt.rcParams['font.size'] = 22

plt.figure()
plt.plot(I*1e3, P*1e3, linewidth = 3, label = 'T = ' + str(temp) + ' °C')
plt.grid(1)
plt.legend()
plt.xlabel('Current (mA)')
plt.ylabel('Power (mW)')

plt.savefig(file_path + "LI_" + str(int(temp)) + "C_COS" + str(cos))

plt.figure()
plt.plot(I*1e3, V, linewidth = 3, label = 'T = ' + str(temp) + ' °C')
plt.grid(1)
plt.legend()
plt.xlabel('Current (mA)')
plt.ylabel('Voltage (V)')

plt.savefig(file_path + "VI_" + str(int(temp)) + "C_COS" + str(cos))

plt.figure()
plt.plot(I*1e3, V*I, linewidth = 3, label = 'T = ' + str(temp) + ' °C')
plt.grid(1)
plt.legend()
plt.xlabel('Current (mA)')
plt.ylabel('Consumed Power (W)')

plt.savefig(file_path + "CP_" + str(int(temp)) + "C_COS" + str(cos))

plt.figure()
plt.plot(I*1e3, T, linewidth = 3, label = 'T = ' + str(temp) + ' °C')
plt.grid(1)
plt.legend()
plt.xlabel('Current (mA)')
plt.ylabel('Measered Temperature (°C)')

plt.savefig(file_path + "T_" + str(int(temp)) + "C_COS" + str(cos))

with open(save_file_path, 'wb') as file:
    pickle.dump(data_dict, file)
    file.close()
    

    
    