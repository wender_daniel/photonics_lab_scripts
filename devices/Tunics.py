# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 16:06:20 2020

@author: Alisson Souza
"""
import visa
#from pyvisa.constants import VI_FALSE, VI_ATTR_SUPPRESS_END_EN,VI_ATTR_SEND_END_EN
class Tunics():
    def __init__(self):
        self.rm = visa.ResourceManager()
        self.laser=self.rm.open_resource("GPIB0::10::INSTR")
#        self.osa=self.rm.open_resource(f"TCPIP0::10.10.213.160::10001::SOCKET")
#        self.laser.set_visa_attribute(VI_ATTR_SUPPRESS_END_EN,VI_FALSE)
#        self.laser.set_visa_attribute(VI_ATTR_SEND_END_EN,VI_FALSE)
        
        print ("Connected to {}".format(self.laser.query('*IDN?')))
    
    def set_frequency(self):
        self.laser.write("SOURce:AM:INTernal:FREQuency MAX;")
    
    def set_wavelentgh_mode(self):
        self.laser.write("SOURce:WAVelength:MODE BASIC;") 

    def set_power_unit(self,unit):
        self.laser.write(f"SOURce:POWer:UNIt {unit};")
        
    def set_power_value(self,pwr):
        self.laser.write(f"SOURce:POWer {pwr}DBM;")
        
    def set_wavelength(self,wl):
        self.laser.write(f"SOURce:WAVelength {wl}NM;")
         
    def set_speed(self,speed):
        self.laser.write(f"SOURce:SPEED {speed}NM/S;")
        
    def set_laser_status(self,on_off):
        self.laser.write(f"SOURce:POWer:STATe {on_off};")
        
    def get_wavelength(self):
        ret=self.laser.query("SOURce:WAVelength?")
        return float(ret.split(",")[0])*1e9
        
    def close(self):
        self.laser.close()
  
if __name__=="__main__":
    tnx=Tunics()
#    tnx.set_frequency()
#    tnx.set_wavelentgh_mode()
#    tnx.set_power_unit('DBM')
#    tnx.set_power_value(0)
#    tnx.set_wavelength(1560)
    print(tnx.get_wavelength())

#    tnx.set_speed(speed='100')
    tnx.set_laser_status(on_off='ON')
    tnx.close()
        

    