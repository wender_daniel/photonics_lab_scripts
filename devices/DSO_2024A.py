# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 09:59:31 2019

@author: Alisson Souza
"""
import numpy as np
import visa 
import time

class Keysight2024A():
    def __init__(self):
        self.rm = visa.ResourceManager()
        self.scope=self.rm.open_resource("USB0::0x0957::0x1796::MY54410245::INSTR")
    
        self.scope.timeout = 15000 # ms #Timeout set to 15000 milliseconds.
        self.scope.encoding = 'latin_1'
        self.scope.read_termination = '\n'
        self.scope.write_termination = None
        self.scope.write('*cls') # clear ESR
        print(self.scope.query("*IDN?"))

    def close(self):
        self.scope.close()
        
    def set_acquire_mode(self,mode,num_AVGs):
        self.scope.write(f":ACQuire:TYPE {mode}") #Average
        ret=self.scope.write(f":ACQuire:COUNt {num_AVGs}")
        print(ret)
        
    def set_trigger_source(self,source):
        ret= self.scope.write(f":TRIGger:EDGE:SOURce {source} ")# EXTernal
        print(ret)

    def set_auto_scale(self):
        self.scope.write(':AUToscale')
        
    def set_time_scale(self,tscale):
        ret=self.scope.write(f":TIMebase:SCALe {tscale}") #Time/division
        print(ret)
    def set_voltage_scale(self,ch,vscale):
        ret=self.scope.write(f":':CHANnel{ch}:SCALe {vscale}'") #Time/division
        print(ret)

    def set_offset(self,ch,value):
        ret=self.scope.write(f":CHANnel{ch}:OFFSet {value}")
        print(ret)
    

    def get_measurement(self,ch, tscale, vscale,MeasType='VPP'):
        self.scope.write(":TIMebase:SCALe {}".format(tscale))   #Time/division
        self.scope.write(':CHANnel{}:SCALe {}'.format(ch,vscale))
        self.scope.write(":CHANnel{}:OFFSet {}".format(ch,3.95*vscale))
        self.scope.write(":MEASure:{} CHANnel{}".format(MeasType,ch))
        Ampl=float(self.scope.query(":MEASure:{}?".format(MeasType)))
        return Ampl

    def set_auto_adjust_scale(self,tscale=10*1E-6, s_ratio=6):
        self.scope.write(':AUToscale')
        time.sleep(1)
        self.scope.write(":TIMebase:SCALe {}".format(tscale)) #Time/division
        scale_ratio=s_ratio
        amp_Ch=np.zeros(5)
        Sc_Ch=np.zeros(5)
    
        for ch in range(1,5):
            if (ch == 4):
                self.scope.write(f":MEASure:Vamplitude CHANnel{ch}")
                amp_Ch[ch]=float(self.scope.query(f":MEASure:VAMPLITUDE? CHANnel{ch}"))
                
            Sc_Ch[ch] = amp_Ch[ch]*(1/scale_ratio)+0.003
            self.scope.write(f':CHANnel{ch}:SCALe {Sc_Ch[ch]}')
            self.scope.write(f":CHANnel{ch}:OFFSet {3.95*Sc_Ch[ch]}")
            self.scope.write(":TRIGger:EDGE:SOURce EXTernal")
    
#        time.sleep(3)
        return Sc_Ch,amp_Ch, scale_ratio

if __name__=="__main__":
    osc=Keysight2024A()
    osc.set_auto_scale()
    osc.set_acquire_mode('Average', 8)
    osc.set_trigger_source('EXTernal')

    ch_scale,ch_amplitude, scale_r=osc.set_auto_adjust_scale(100*1E-6,7)
    print(ch_scale[4],ch_amplitude[4], scale_r)

#    print(ch_scale[4]*1000,ch_amplitude[4]*1000, scale_r)
    
    amp=osc.get_measurement(ch=4, tscale=50*1E-6, vscale=ch_scale[4],MeasType='Vamplitude')
    print(f'{amp}\n')


#    osc.close()
