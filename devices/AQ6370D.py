#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
http://obswww.unige.ch/people/bruno.chazelas/optlab/r/repo/b/master/t/DEVICES/Yokogawa/f=AQ6370C.py.html
Created on Tue Sep 10 00:16:03 2019

@author: Alisson Souza
"""

import visa
import numpy as np
from pyvisa.constants import VI_FALSE, VI_ATTR_SUPPRESS_END_EN,VI_ATTR_SEND_END_EN, VI_TRUE,  VI_ATTR_TERMCHAR_EN 
class AQ6370D():
    def __init__(self):
        self.rm = visa.ResourceManager()
        self.osa=self.rm.open_resource("GPIB0::21::INSTR")#("GPIB0::21::INSTR")
#        self.osa.timeout = 40000 # ms #Timeout set to 15000 milliseconds.
#        self.osa=self.rm.open_resource(f"TCPIP0::10.10.213.160::10001::SOCKET")
#        self.osa.set_visa_attribute(VI_ATTR_SUPPRESS_END_EN,VI_FALSE)
#        self.osa.set_visa_attribute(VI_ATTR_SEND_END_EN,VI_FALSE)
        self.osa.set_visa_attribute(VI_ATTR_TERMCHAR_EN,VI_FALSE)
#        self.osa.write('open "cpqd"')
#        b=self.osa.read()
#        self.osa.write('cpqd')
#        c=self.osa.read() 
#        self.osa.read_termination = '\n'
#        self.osa.write_termination = '\n'
        self.osa.write('*cls')
        print ("Connected to {}".format(self.osa.query('*IDN?')))
        
    def set_center(self,lamb):
        self.osa.write(":sens:wav:cent {}nm".format(lamb)) #sweep center wl
        
    def set_span(self,dlamb):
        self.osa.write(":sens:wav:span {}nm".format(dlamb)) #sweep span
    
    def set_resolution(self,res):
        ret=self.osa.write(f":SENSe:BANDwidth:RES {res}nm")
        print(ret)
        
    def set_sens_mode(self,mode):
        self.osa.write(f":sens:sens {mode}") #sens mode = HIGH1 
        
    def sampling_points(self,mode):
        self.osa.write(f":sens:sweep:points:{mode}") #Sampling Point = AUTO ON /9999
#        self.osa.write(f":sens:sweep:points:{n}") #Sampling Point = AUTO ON /9999
#    def sampling_points_n(self,mode):
#        self.osa.write(f":sens:sweep:segment:points:{mode}") #Sampling Point = AUTO ON /9999

    def set_sweep_mode(self,mode):
        self.osa.write(f":init:smode: {mode}") #single sweep mode
        self.osa.write("*CLS")  #status clear
        
    def start(self):
        self.osa.write(":init") #sweep start
        
    def wait_for_scan(self):
        re="0" #Bit0: Sweep status
        while "0" in re:
            re=self.osa.query(":stat:oper:even?") #get Operation Event Register

    def set_start_wl(self,start):
        self.osa.write(f":sens:wav:start {start}nm")
        
    def set_stop_wl(self,stop):
        self.osa.write(f":sens:wav:stop {stop}nm")
        
    def set_display_status(self,status):  
        self.osa.write(f":DISPLAY {status}") #  ON /  OFF / 0 / 1 

    def get_start_wl(self):
        ret=self.osa.query(f":sens:wav:start?")
        return float(ret[1:-1])*1e9

    def get_stop_wl(self):
        ret=self.osa.query(f":sens:wav:stop?")       
        return float(ret[1:-1])*1e9
        
    def analysis_power(self):
        self.osa.write(":calc:category pow") # Power analysis
        self.osa.write(":calc")     #Analysis Execute
        ret=self.osa.query(":calc:data?")#get data
        return float(ret.split(",")[0])*1e9,float(ret.split(",")[1])*1e9 #ret[0]*1e9,ret[1]*1e9 #
    
    def analysis_THRESH(self):
        self.osa.write(":calc:category swth")  #Spectrum width analysis(THRESH type)
        self.osa.write(":calc")    #Analysis Execute
#        ret=np.array(self.osa.query(":calc:data?").split(','), dtype = float)    #get data
        ret=self.osa.query(":calc:data?")
        return float(ret.split(",")[0])*1e9,float(ret.split(",")[1])*1e9
    
    def analysis_SMSR(self):
        self.osa.write(":calc:category SMSR") #  Side Mode Suppression Ratio
        self.osa.write(":calc")     #Analysis Execute
        ret=self.osa.query(":calc:data?") #get data
        return float(ret.split(",")[0])*1e9,float(ret.split(",")[1])*1e9#float(ret.split(",")[0])*1e9,float(ret.split(",")[1])
        
    def get_traceX(self,trace):
        ret= np.array(self.osa.query(f':TRACE:X? TR{trace} ').split(','), dtype = float)
        return ret
    def get_traceY(self,trace):
        ret= np.array(self.osa.query(f':TRACE:Y? TR{trace} ').split(','), dtype = float)
        return ret
        
    def close(self):
        self.set_zeroing('ON')
#        self.set_zeroing('ONCE')
        self.osa.close()


        
    def set_trigger_status(self,status):  
        self.osa.write(f"TRIGER:STATE {status}") #  External Trigger  
    
    def set_zeroing(self,status):
        self.osa.write(f":CALibration:ZERO {status}") #ONCE, 0 or 1 

  
if __name__=="__main__":
    aq=AQ6370D()
    aq.set_center(1550)
#    aq.set_trigger_status('ON')
    aq.set_span(55)
    aq.set_resolution(0.02)
#    aq.get_start_wl()
#    aq.set_stop_wl(1598)
    aq.set_sens_mode('MID')
#    aq.sampling_points("AUTO OFF")
    aq.set_sweep_mode(1) # 1:Single mode, 2:Repeat mode
    aq.start()
    aq.wait_for_scan()
    print(aq.get_traceX('E'))
    print(aq.get_traceY('E'))
    print(aq.analysis_power())
    print(aq.analysis_THRESH())
    print(aq.analysis_SMSR())
  
    
    aq.close()
