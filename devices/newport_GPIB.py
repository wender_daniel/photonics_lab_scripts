"""
Newport Optical Power Meter 1830-C
Control and Operation using GPIB interface

@copyright Idea! Electronic Systems
@author Vinicius Cruz <vinicius.cruz@idea-ip.com>

Based on previous code done by:
    Jonyelison Morais Alves <jonyelison.alves@idea-ip.com>
    Diogo de Azevedo Motta <diogo.motta@idea-ip.com>
    Felipe Bizerra Fideles <felipe.fideles@idea-ip.com>
"""

import visa
import time

class Photodetector(object):
    '''Newport Photodetector's Class'''

    def __init__(self, resource):
        self.res_mgmt = visa.ResourceManager()
        self.device = self.res_mgmt.open_resource(resource)
        self.wait_time = 0.1

    def __del__(self):
        self.device.close()
    
    def status(self):
        '''Retrieve status register data'''
        time.sleep(self.wait_time)
        self.device.query("Q?\n")
        time.sleep(self.wait_time)
        return int(self.device.query("Q?\n"))

    def get_power(self):
        '''Retrieve external photodetector's power'''
        try:
            time.sleep(self.wait_time)
            self.device.query("D?\n")
            time.sleep(self.wait_time)
            out = float(self.device.query("D?\n")) 
            unit = self.get_unit()
            if unit == 'W':
                while out*1e3 > 100:
                    time.sleep(self.wait_time)
                    out = float(self.device.query("D?\n"))
            elif unit == 'dB' or unit == 'dBm':
                while out > 50:
                    time.sleep(self.wait_time)
                    out = float(self.device.query("D?\n"))
        except:
            time.sleep(self.wait_time)
            out = float(self.device.query("D?\n"))
        return out

    def set_dbm(self):
        '''Change external photodetector's unit to dBm'''
        self.device.write("U3\n")
        
    def set_w(self):
        '''Change external photodetector's unit to W'''
        self.device.write("U1\n")
        
    def set_db(self):
        '''Change external photodetector's unit to dB'''
        self.device.write("U2\n")
        
    def lockout(self):
        '''Lockout front panel control'''
        self.device.write("L1\n")
        
    def get_unit(self):
        time.sleep(self.wait_time)
        self.device.query("U?\n")
        time.sleep(self.wait_time)
        unit = int(self.device.query("U?\n"))
        if unit == 1:
            return 'W'
        elif unit == 2:
            return 'dB'
        elif unit == 3:
            return 'dBm'

    def averaging(self, _filter):
        ''' Define averaging between 1, 4 or 16 samples'''
        if _filter=='Fast':
            self.device.write("F3\n")
        elif _filter=='Medium':
            self.device.write("F2\n")
        elif _filter=='Slow':
            self.device.write("F1\n")