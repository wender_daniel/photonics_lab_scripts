# -*- coding: utf-8 -*-
"""
Created on Fri Apr 12 14:13:08 2019

@author: jony
"""

#from functions import Handling_Folders_and_Files3 as hff 
from Thorlabs_PM import Photodetector
# from devices.ITC4001 import ITC400X
from devices.Tunics import Tunics
import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np
import time
import pickle
# import os
   
        
def TunicsSweep(speed = 1, start = 1510, stop = 1580, step = 0.5, points = 5, temp = 25, reverse = False, p_ref = 0):
    try:
        
        deg = ['0 deg', '4 deg', '6 deg', '10 deg']
        path_file = r'C:\Users\idea\Desktop\PS_measurements'
        plt.figure()
        for ang in deg:
            print('Ready?')
            input()
            laser = Tunics()
            power_meter = Photodetector('W', 'LOW', visastr = 'USB0::0x1313::0x8078::P0009663::INSTR')
            power_meter.set_unit('W')
            # power_meter=Photodetector(visastr='USB0::0x1313::0x8078::P0024375::INSTR')
            # ld=ITC400X()
        
            power_meter.set_unit('DBM')
        
            laser.set_frequency()
            laser.set_wavelentgh_mode()
            laser.set_power_unit('DBM')
            laser.set_power_value('5')
            # laser.set_speed(speed='100')
            laser.set_laser_status(on_off = 'ON')
              
            
            wl = np.arange(start, stop + step, step)
            if reverse:
                wl = wl[::-1]
            
            powerMean = np.array([], dtype = float)
            
            laser.set_wavelength(wl = wl[0])
            
            # ld.set_TEC_status("ON")
            # ld.set_temp(temp)  
    
            time.sleep(5)
            # t0 = time.time()
            
            measure={}
            info={}
            Test_name = 'Phase_Shifter_Caracterization'
    #        lvl='COC' 
            Test='Phase_shifter2'
            Signal='CW'
    #        batch_waf=12345
    #        part_num='S00X'
    #        chip_p_n='asdafg'
        
    #        ld.set_mode_CW('DC')
    #        ld.set_LD_status('ON')
    
            
    
    #            time.sleep(2) 
            
            # current date and time
            now = datetime.now()
            #date = now.strftime("%m-%d-%Y")
            date = now.strftime("%b-%d-%Y")
            Time = now.strftime("%Hh%Mmin%Ss")
            # dd/mm/YY H:M:S format
            print(f"date: {date}\nTime: {Time}")
            
            info[Test_name]= Test_name
            info["Date"] = date
            info["Time"] = Time
    #            info[f"Test_Level"]=lvl
            info[Test]=Test
            info["Current-Type"]= Signal 
            measure.update(info)
        
        
            
                    
            wvl = np.array([], dtype = float)
            pwr = np.array([], dtype = float)
    
            for i in wl:
                laser.set_wavelength(np.round(i, 4))
                
                for j in range(points):
                    time.sleep(0.05)
                    powerMean = np.append(powerMean, [float(power_meter.get_power())])
                    
               
                wvl = np.append(wvl, i)
                pwr = np.append(pwr, powerMean[-1])
                
                string = f'Wavelength: {wvl[-1]:.2f} Power: {pwr[-1]:.2f}' 
                print(string)
                powerMean = []
                time.sleep(0.05)
                
    
            laser.set_laser_status(on_off='OFF')
    
            pwr = pwr - p_ref
            
            plt.plot(wvl, pwr, linewidth = 2, label = ang)
            
            data_dict = {'power': pwr,
                         'wavelength': wvl
                         }
            output = open(path_file + '\\' + Test + date + Time + '.pkl', 'wb')
            pickle.dump(data_dict, output)
            output.close()
        
        plt.rcParams.update({'font.size': 22})
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Power [dBm]')
        plt.grid(1)
        plt.xlim([start,stop])
        plt.title('Phase Shifter Filter Spectrum')
        plt.legend()
        plt.show()
        PS_name = 'PS3'
        plt.savefig(path_file + f'\\{PS_name}.png')
        
    except KeyboardInterrupt:
        
        laser.set_laser_status(on_off='OFF')
        # ld.set_TEC_status("OFF")
        print('interrupted by user')
        return data_dict
        
    return data_dict

if __name__ == "__main__":
    
    data = np.load('C:/Users/idea/Desktop/PS_measurements/Phase_shifter2Nov-10-202015h38min00s.pkl', allow_pickle = 1)
    x = TunicsSweep(step = 0.2, p_ref = data['power'])

   